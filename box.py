class Box:
	def __init__(self, capacity=None, is_open=None):
		self.capacity = capacity
		self.is_open = is_open

	@staticmethod
	def from_data(data):
		c=data.get("capacity", None)
		i=data.get("is_open", None)
		return Box(capacity=c, is_open=i)
