class Person: 
	def __init__(self, firstname=None, lastname=None):
		self.firstname = firstname
		self.lastname = lastname
		
	@staticmethod
	def from_data(data):
		f = data.get("firstname", None)
		l = data.get("lastname", None)
		return Person(firstname=f, lastname=l)
