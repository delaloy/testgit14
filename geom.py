class point:
	def __init__(self, x, y):
		self.x = x
		self.y = y
		
	def getx(self):
		return self.x
		
	def gety(self):
		return self.y
		
	def __repr__(self):
		return "<point %s,%s>" % (self.x, self.y)
		
	def __eq__(self, other):
		return self.x==other.x and self.y==other.y

class rectangle:
	def __init__(self, x, y, lo, la):
		self.p=point(x,y)
		self.longueur = lo
		self.largeur = la
		
		
	def getx(self):
		return self.p.getx()
		
	def gety(self):
		return self.p.gety()
		
	def get_longueur(self):
		return self.lo
		
	def get_largeur(self):
		return self.la
		
	def get_center(self):
		return "<point %s,%s>" % (self.p.getx()+self.la/2, self.p.gety()+self.lo/2))

	def get_surface(self):
		return self.lo * self.la
		
	def get_centre(self):
		return print(self.p.getx()-(self.
