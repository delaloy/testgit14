import yaml
import io

text = """
- is_open: true
  capacity: 3
- is_open: false
  capacity: 5
"""

stream = io.StringIO(text)
l = yaml.load(text)

